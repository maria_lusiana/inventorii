<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/article/{id}','BarangController@showArticle')->where('id','[0-9]+');
Route::get('/','BarangController@index');
Route::get('/barang/{id}','BarangController@show');
Route::get('/kategori','KategoriController@index');
Route::get('/kategori/{id}','KategoriController@show');
Route::get('/transaksi','TransaksiController@index');
Route::get('/transaksi/{id}','TransaksiController@show');
Route::get('/detail','DetailController@index');
Route::get('/detail/{id}','DetailController@show');
Route::get('/pembelian','BarangMasukController@index');
Route::get('/pembelian/{id}','BarangMasukController@show');
Route::get('/pembeliandetail','MasukDetailController@index');
Route::get('/pembeliandetail/{id}','MasukDetailController@show');
Route::get('/distributor','DistributorController@index');
Route::get('/distributor/{id}','DistributorController@show');