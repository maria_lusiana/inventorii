<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BarangMasuk;
use App\Http\Requests;
use App\Distributor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;


class BarangMasukController extends Controller
{
    function index(){
        $result = Redis::Connection();
        $result= Redis::get('pembelian');
        if ($result) {
            return $result;
        }else{
            $result= DB::table('tb_barang_masuk')
            ->select(DB::raw('tb_barang_masuk.id,tb_barang_masuk.id_distributor,sum(if(tb_barang_masuk.id=tb_detail_masuk.id_notabarang,subtotal,0)) as total_bayar'))
             ->join('tb_detail_masuk','tb_barang_masuk.id','=','tb_detail_masuk.id_notabarang')
             ->groupBy('tb_barang_masuk.id')
             ->get();
           
            $pembelian=Redis::set('pembelian',$result);
            return $result;
        }
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'id_distributor'=>'numeric',
        ]);
        $id_distributor = $request->input('id_distributor');

        $distributor=Distributor::find($id_distributor);
        if ($distributor){
            Redis::del('pembelian');
            $barangmasuk = BarangMasuk::create($request->all());
            return response()->json($barangmasuk, 201); 
        }
            return response()->json(['status' => 'error', 'message' => 'ID Distributor not found'],404);
    }

    public function show($id)
    {
        $result = Redis::Connection();
        
        $result= Redis::get('pembelian:'.$id);
        if ($result) {
            return $result;
        }else{
            $pembelian=BarangMasuk::find($id);
            if($pembelian){
                $result= DB::table('tb_barang_masuk')
                ->select(DB::raw('tb_barang_masuk.id,tb_barang_masuk.id_distributor,sum(if(tb_barang_masuk.id=tb_detail_masuk.id_notabarang,subtotal,0)) as total_bayar'))
                ->join('tb_detail_masuk','tb_barang_masuk.id','=','tb_detail_masuk.id_notabarang')
                 ->where('tb_barang_masuk.id','=', $id)
                 ->get();
    
                $pembelian=Redis::set('pembelian:'.$id,$result);
                return response()->json(['status' => 'success', 'data' => $result]);
            }
            return response()->json(['status' => 'error', 'message' => ' ID not found'],404);
        }
      
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'id_distributor'=>'numeric',
        ]);

        $id_distributor = $request->input('id_distributor');

        $distributor=Distributor::find($id_distributor);
        $barangmasuk = BarangMasuk::find($id);

        if(!$barangmasuk){
            return response()->json(['status' => 'error', 'message' => 'Data not found'],404);
        }
        if ($distributor){
            Redis::del('pembelian:'.$id);
            Redis::del('pembelian');
            $barangmasuk->update($request->all());
            return response()->json($barangmasuk, 200);
        }
            return response()->json(['status' => 'error', 'message' => 'ID Distributor not found'],404);
    }

   
    public function destroy($id)
    {
      $barangmasuk = BarangMasuk::find($id);
      if ($barangmasuk) {
        Redis::del('pembelian:'.$id);
        Redis::del('pembelian');
        $barangmasuk->delete();
        return response()->json(['stats' => 'success', 'message' => 'Data has been deleted']);
      }
      return response()->json(['status' => 'error', 'message' => 'Cannot deleting data'],400);
    }
}
