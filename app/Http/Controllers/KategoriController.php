<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriModel;
use App\Http\Requests;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class KategoriController extends Controller
{
    function index(){
      
        Redis::Connection();
        $kategori=Redis::get('kategori');
       
       if ($kategori !=null) {
          return $kategori;
         }  
          $kategori=Redis::set('kategori',KategoriModel::all());
          return KategoriModel::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_kategori'=>'required|max:255',
            'deskripsi'=>'required|max:255',
        ]);
        Redis::del('kategori');
        $kategori = KategoriModel::create($request->all());
         
        return response()->json($kategori, 201);  
    }

    public function show($id)
    {
        $result= Redis::get('kategori:'.$id);
        if ($result) {
            return $result;
        }else{
            $result = KategoriModel::find($id);

            $kategori=Redis::set('kategori:'.$id,$result);
            return response()->json(['status' => 'success', 'data' => $result]);
        }
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'nama_kategori'=>'required|max:255',
            'deskripsi'=>'required|max:255',
        ]);
        $kategori = KategoriModel::find($id);

        if(!$kategori){
            return response()->json(['status' => 'error', 'message' => 'ID Kategori not found'],404);
        }
            Redis::del('kategori');
            Redis::del('kategori:'.$id);
            $kategori->update($request->all());
            return response()->json($kategori, 200);
    }

    public function destroy($id)
    {
      $kategori = KategoriModel::find($id);
      if ($kategori) {
        Redis::del('kategori');
        Redis::del('kategori:'.$id);
        $kategori->delete();
        return response()->json(['stats' => 'success', 'message' => 'Data has been deleted']);
      }
 
      return response()->json(['status' => 'error', 'message' => 'Cannot deleting data'],400);
    }


}

