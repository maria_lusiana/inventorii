<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Barang;
use App\KategoriModel;

class Barang extends Model
{
    public $timestamps = false;
    public $table="product";
    protected $fillable = 

    ['id', 'nama_barang', 'harga','stok','id_kategori'];


    //relasi many to one
    public function get_kategori(){
    return $this->belongsTo('App\KategoriModel','id','id');
    }


}