<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaksi;

class Transaksi extends Model
{
    public $table="transaksi";
    public $timestamps = false;
    protected $fillable = 
    ['id', 'tgl_transaksi'];

    //relasi one to  many
    public function get_detail(){
    return $this->hasMany('App\Detail','id_transaksi','id');
    
    }

}