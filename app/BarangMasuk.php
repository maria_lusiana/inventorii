<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BarangMasuk;


class BarangMasuk extends Model
{
    public $table="tb_barang_masuk";
    public $timestamps = false;
    protected $fillable = 
    ['id', 'id_distributor', 'total'];

    //relasi one to many
    public function get_detailmasuk(){
    return $this->hasMany('App\DetailMasuk','id','id');
}

}