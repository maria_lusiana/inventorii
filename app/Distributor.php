<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Distributor;


class Distributor extends Model
{
    public $table="tb_distributor";

    public $timestamps = false;
    protected $fillable = 
    ['id', 'nama_distributor', 'alamat','kota','email','telepon'];

    //relasi many to one
    public function get_barangmasuk(){
    return $this->hasMany('App\Barangmasuk','id','id');
}

}
